package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
	"logistekBox/logistekLib"
	"path"
	"strconv"
)

type Command struct {
	Code    int
	Message string
}

func newCommand(code int, msg string) Command {
	return Command{
		Code:    code,
		Message: msg,
	}
}

//----------------------------------------------------------------------

type MainController struct {
	beego.Controller
}

func (this *MainController) Demo() {
	this.TplNames = "demo.tpl"
}

//测试服务器是否正常
func (this *MainController) Test() {
	this.Data["json"] = newCommand(0, "")
	this.ServeJson()
}

//----------------------------------------------------------------------

func (this *MainController) Overview() {
	this.TplNames = "overview.tpl"
}

func (this *MainController) Index() {
	// this.Data["systemName"] = "物联网实验信息管理系统"
	// this.Data["companyName"] = "北京科技公司"
	this.TplNames = "OnlineUsersIndex.tpl"
}
func (this *MainController) OnlineUsersIndex() {
	this.TplNames = "OnlineUsersIndex.tpl"
}
func (this *MainController) TrainingProcessesIndex() {
	this.TplNames = "TrainingProcessesIndex.tpl"
}

func (this *MainController) TrainingResultsIndex() {
	this.TplNames = "TrainingResultsIndex.tpl"
}
func (this *MainController) DesktopSharesIndex() {
	this.TplNames = "DesktopSharesIndex.tpl"
}

//----------------------------------------------------------------------

func (this *MainController) TrainingResults() {
	this.Data["json"] = logistekLib.TrainingResults
	this.ServeJson()
}
func (this *MainController) RemoveTrainingResults() {

	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	data := this.GetString("data")
	// fmt.Println("data =>")
	// fmt.Println(data)
	if err := logistekLib.RemoveTrainingResults(data); err != nil {
		cmd = newCommand(1, "删除实训结果失败")
		return
	}
}
func (this *MainController) AddTrainingResult() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		cmd = newCommand(1, "ID为空")
		return
	}
	processName := this.GetString("name")
	if len(processName) <= 0 {
		cmd = newCommand(1, "实训进程名称不能为空")
		return
	}
	result := this.GetString("result")
	if len(result) <= 0 {
		cmd = newCommand(1, "实训结果不能为空")
		return
	}

	if err := logistekLib.AddTrainingResult(ID, processName, result); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

//----------------------------------------------------------------------

func (this *MainController) TrainingProcesses() {
	this.Data["json"] = logistekLib.TrainingProcesses
	this.ServeJson()
}

func (this *MainController) AddTrainingProcess() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		cmd = newCommand(1, "ID为空")
		return
	}
	processName := this.GetString("name")
	if len(processName) <= 0 {
		cmd = newCommand(1, "实训进程名称不能为空")
		return
	}
	if err := logistekLib.AddTrainingProcess(ID, processName); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

func (this *MainController) RemoveTrainingProcess() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	data := this.GetString("data")
	// fmt.Println("data =>")
	// fmt.Println(data)
	if err := logistekLib.RemoveTrainingProcess(data); err != nil {
		cmd = newCommand(1, "删除实训流程失败")
		return
	}
}

//----------------------------------------------------------------------

func (this *MainController) DesktopShares() {
	this.Data["json"] = logistekLib.DesktopShares
	this.ServeJson()
}
func (this *MainController) UploadDesktopShare() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		cmd = newCommand(1, "ID为空")
		return
	}
	fromFile := "picname"
	_, header, err := this.GetFile(fromFile)
	if err != nil {
		logistekLib.DebugSys(err.Error() + logistekLib.GetFileLocation())
	} else {
		if header != nil {
			// logistekLib.DebugInfo(header.Filename)
			ext := path.Ext(header.Filename)
			count := logistekLib.GetMaxDesktopShareCount() + 1
			fileName := ID + "-" + strconv.Itoa(count) + ext
			if err := this.SaveToFile(fromFile, "static/desktopscreen/"+fileName); err != nil {
				logistekLib.DebugSys("保存图片出错：" + err.Error() + logistekLib.GetFileLocation())
				cmd = newCommand(1, "上传图片出错")
				return
			} else {
				logistekLib.AddDesktopShare(ID, fileName, count)
				logistekLib.DebugInfo(fmt.Sprintf("编号 %s 上传了图片 %s", ID, header.Filename))
			}
		} else {
			logistekLib.DebugInfo("没有图片上传")
			cmd = newCommand(1, "上传图片出错")
			return
		}
	}
}

//----------------------------------------------------------------------

func (this *MainController) Online() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	IP := this.GetString("IP")

	if len(ID) <= 0 {
		cmd = newCommand(1, "ID为空")
		return
	}
	if len(IP) <= 0 {
		cmd = newCommand(1, "没有IP地址")
		return
	}
	if err := logistekLib.UserOnline(ID, IP); err != nil {
		cmd = newCommand(1, err.Error())
		return
	}
}

func (this *MainController) Offline() {
	cmd := newCommand(0, "")
	defer func() {
		this.Data["json"] = cmd
		this.ServeJson()
	}()
	ID := this.GetString("ID")
	if len(ID) <= 0 {
		this.Data["json"] = newCommand(1, "ID为空")
		return
	}
	logistekLib.UserOffline(ID)
}

func (this *MainController) UserList() {
	this.Data["json"] = logistekLib.UsersOnline
	this.ServeJson()
}
