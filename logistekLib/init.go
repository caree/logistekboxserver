package logistekLib

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego/config"
	"github.com/codegangsta/cli"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	USER_OFF_LINE_INTERVAL time.Duration = 30 //second
)

var (
	UsersOnline         UserList               = UserList{}            //在线用户列表
	TrainingProcesses   TrainingProcessList    = TrainingProcessList{} //实验进程列表
	TrainingResults     TrainingResultList     = TrainingResultList{}  //实验结果列表
	DesktopShares       DesktopShareList       = DesktopShareList{}
	UserOfflineInterval                        = USER_OFF_LINE_INTERVAL * time.Second //用户自动掉线默认时长
	cliApp              *cli.App                                                      //交互线程
	iniconf             config.ConfigContainer = nil
)

func init() {
	initConfig()
	go initCli()

	go initUserOfflineCheck(UserOfflineInterval)
}

//----------------------------------------------------------------------
//
//添加一个实验结果
func AddTrainingResult(id, name, result string) error {
	if isOnline(id) == false {
		return errors.New("该客户端已经下线")
	}
	TrainingResults = TrainingResults.Add(id, name, result)
	DebugInfo(fmt.Sprintf("添加一个实训结果 ID：%s  名称：%s 结果：%s", id, name, result) + GetFileLocation())
	return nil
}
func RemoveTrainingResults(results string) error {
	var trainingResults TrainingResultList
	if err := json.Unmarshal([]byte(results), &trainingResults); err != nil {
		return err
	} else {
		TrainingResults = TrainingResults.Remove(trainingResults)
		DebugInfo("删除实训结果成功")
		return nil
	}
}

//----------------------------------------------------------------------

//添加一个实验进程
func AddTrainingProcess(id, name string) error {
	if isOnline(id) == false {
		return errors.New("该客户端已经下线")
	}
	TrainingProcesses = TrainingProcesses.Add(id, name)
	DebugInfo(fmt.Sprintf("添加一个实验进程 ID：%s  名称：%s", id, name) + GetFileLocation())
	return nil
}
func RemoveTrainingProcess(data string) error {
	var processes TrainingProcessList
	if err := json.Unmarshal([]byte(data), &processes); err != nil {
		return err
	} else {
		TrainingProcesses = TrainingProcesses.Remove(processes)
		DebugInfo("删除实训流程成功")
		return nil
	}
}

//----------------------------------------------------------------------

//
//桌面共享
func AddDesktopShare(id, file string, count int) error {
	DebugInfo(fmt.Sprintf("添加一个屏幕共享 ID：%s ", id) + GetFileLocation())
	DesktopShares = DesktopShares.Add(id, file, count)
	return nil
}
func GetMaxDesktopShareCount() int {
	if len(DesktopShares) > 0 {
		return DesktopShares[len(DesktopShares)-1].lastCount
	}
	return 1
}

//----------------------------------------------------------------------

//
//实验箱联网
func UserOffline(id string) {
	UsersOnline = UsersOnline.Remove(id)
	DebugInfo(fmt.Sprintf("编号 %s 实验箱下线", id) + GetFileLocation())
}
func UserOnline(id, ip string) error {
	var err error
	UsersOnline, err = UsersOnline.Add(id, ip)
	if err != nil {
		DebugInfo(fmt.Sprintf("编号 %s (IP: %s)实验箱上线失败：%s", id, ip, err) + GetFileLocation())
	} else {
		DebugInfo(fmt.Sprintf("编号 %s (IP: %s)实验箱上线", id, ip) + GetFileLocation())
	}
	return err
}

func isOnline(id string) bool {
	for _, user := range UsersOnline {
		if user.ID == id {
			return true
		}
	}
	return false
}

func initUserOfflineCheck(d time.Duration) {
	funcCheckUserOffline := func() {
		UsersOnlineTemp := UsersOnline
		now := time.Now()
		for _, u := range UsersOnlineTemp {
			if now.Sub(u.lastOnlineTime) > d {
				UsersOnline = UsersOnline.Remove(u.ID)
				DebugInfo(fmt.Sprintf("编号 %s 实验箱超时下线", u.ID) + GetFileLocation())
			}
		}
	}
	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ticker.C:
			funcCheckUserOffline()
		}
	}
}

func initConfig() {
	var err error
	iniconf, err = config.NewConfig("ini", "conf/app.conf")
	if err != nil {
		DebugMust(err.Error() + GetFileLocation())
	} else {
		if intervalTemp, err := iniconf.Int("UserOfflineInterval"); err != nil {
			DebugMust(err.Error() + GetFileLocation())

		} else {
			if intervalTemp > 0 {
				DebugInfo(fmt.Sprintf("获取自动下线时间间隔：%d 秒", intervalTemp) + GetFileLocation())
				UserOfflineInterval = time.Duration(intervalTemp) * time.Second
			} else {
				DebugInfo(fmt.Sprintf("获取自动下线时间间隔为默认值：%d 秒", UserOfflineInterval) + GetFileLocation())

			}
		}
		// if err := iniconf.Set("locationCount", "23"); err != nil {
		// 	DebugMust(err.Error() + GetFileLocation())
		// }
		// if err := iniconf.SaveConfigFile("conf/app.conf"); err != nil {
		// 	DebugMust(err.Error() + GetFileLocation())
		// }
	}

}

func initCli() {
	cliApp := cli.NewApp()
	cliApp.Name = "config"
	cliApp.Usage = "设置系统运行参数"
	cliApp.Version = "1.0.1"
	cliApp.Email = "ssor@qq.com"
	cliApp.Commands = []cli.Command{
		{
			Name:        "log",
			ShortName:   "l",
			Usage:       "是否打印log，true为打开，false为关闭",
			Description: "当log太多，而需要与系统交互时可以关闭log的打印",
			Action: func(c *cli.Context) {
				// fmt.Println(fmt.Sprintf("%#v", c.Command))
				// fmt.Println("-----------------------------")
				value := strings.ToLower(c.Args().First())
				if value == "true" {
					fmt.Println("打开log打印")
					G_printLog = true
				} else if value == "false" {
					fmt.Println("关闭log打印")
					G_printLog = false
				} else {
					fmt.Println("参数错误")
				}
			},
		}, {
			Name:        "loglevel",
			ShortName:   "ll",
			Usage:       "设置打印log级别，数字越高，log打印越精细，默认值为3",
			Description: "当需要调试时，可以设置高级别打印",
			Action: func(c *cli.Context) {
				value := strings.ToLower(c.Args().First())
				if level, err := strconv.Atoi(value); err != nil {
					fmt.Println("参数错误")
				} else {
					DebugLevel = level
					fmt.Println("设置log打印级别为" + value)
				}
			},
		},
	}
	go func() {
		reader := bufio.NewReader(os.Stdin)
		for {
			fmt.Println("等待输入。。。")

			data, _, _ := reader.ReadLine()
			command := string(data)
			cliApp.Run(strings.Split(command, " "))
		}
	}()
	// app.Run(os.Args)
}
