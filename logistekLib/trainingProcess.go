package logistekLib

import (
	// "errors"
	"time"
)

type TrainingProcess struct {
	ID          string
	Time        string
	ProcessName string
}

func NewTrainingProcess(id, name string) *TrainingProcess {
	return &TrainingProcess{
		ID:          id,
		ProcessName: name,
		Time:        time.Now().Format("2006-01-02 15:04:05"),
	}
}

type TrainingProcessList []*TrainingProcess

func (this TrainingProcessList) Add(id, name string) TrainingProcessList {
	return append(this, NewTrainingProcess(id, name))
}

func (this TrainingProcessList) Contains(process *TrainingProcess) bool {
	for _, _process := range this {
		if _process.ID == process.ID && _process.Time == process.Time {
			return true
		}
	}
	return false
}

func (this TrainingProcessList) Remove(list TrainingProcessList) TrainingProcessList {
	tempList := TrainingProcessList{}
	for _, process := range this {
		if list.Contains(process) == false {
			tempList = append(tempList, process)
		}
	}
	return tempList
}
