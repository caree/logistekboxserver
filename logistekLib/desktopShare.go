package logistekLib

import (
	// "errors"
	"time"
)

type DesktopShare struct {
	ID        string
	Time      string
	File      string
	lastCount int
}

func NewDesktopShare(id, file string, count int) *DesktopShare {
	return &DesktopShare{
		ID:        id,
		File:      file,
		lastCount: count,
		Time:      time.Now().Format("2006-01-02 15:04:05"),
	}
}

type DesktopShareList []*DesktopShare

func (this DesktopShareList) Add(id, file string, count int) DesktopShareList {
	return append(this, NewDesktopShare(id, file, count))
}
