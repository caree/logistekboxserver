package logistekLib

import (
	// "errors"
	"time"
)

type TrainingResult struct {
	ID           string
	Time         string
	TrainingName string
	Result       string
}

func NewTrainingResult(id, name, result string) *TrainingResult {
	return &TrainingResult{
		ID:           id,
		TrainingName: name,
		Result:       result,
		Time:         time.Now().Format("2006-01-02 15:04:05"),
	}
}

type TrainingResultList []*TrainingResult

func (this TrainingResultList) Add(id, name, result string) TrainingResultList {
	return append(this, NewTrainingResult(id, name, result))
}

func (this TrainingResultList) Remove(list TrainingResultList) TrainingResultList {
	tempResults := TrainingResultList{}
	for _, result := range this {
		if list.Contains(result) == false {
			tempResults = append(tempResults, result)
		}
	}
	return tempResults
}

func (this TrainingResultList) Contains(result *TrainingResult) bool {
	for _, _result := range this {
		if _result.ID == result.ID && _result.Time == result.Time {
			return true
		}
	}
	return false
}
