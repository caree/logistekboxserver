package logistekLib

import (
	"errors"
	"time"
)

type User struct {
	ID              string
	FirstOnlineTime string
	firstOnlineTime time.Time
	LastOnlineTime  string
	lastOnlineTime  time.Time
	IP              string
}

func NewUser(id, ip string, time time.Time) *User {
	return &User{
		ID:              id,
		firstOnlineTime: time,
		lastOnlineTime:  time,
		FirstOnlineTime: time.Format("2006-01-02 15:04:05"),
		LastOnlineTime:  time.Format("2006-01-02 15:04:05"),
		IP:              ip,
	}
}

//----------------------------------------------------------------------

type UserList []*User

func (this UserList) Remove(id string) UserList {
	for i, u := range this {
		if u.ID == id {
			return append(this[0:i], this[i+1:]...)
		}
	}
	return this
}
func (this UserList) Add(id, ip string) (UserList, error) {
	var user *User = nil
	for _, u := range this {
		if u.ID == id {
			user = u
			break
		}
	}
	now := time.Now()
	if user == nil {
		this = append(this, NewUser(id, ip, now))
	} else {
		if user.IP != ip { //不会刷新最后登录时间
			return this, errors.New("重复登录")
		}
		user.lastOnlineTime = now
		user.LastOnlineTime = now.Format("2006-01-02 15:04:05")
	}
	return this, nil
}
