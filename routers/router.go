package routers

import (
	"github.com/astaxie/beego"
	"logistekBox/controllers"
)

func init() {

	// beego.Router("/", &controllers.MainController{}, "get:Demo")
	beego.Router("/", &controllers.MainController{}, "get:Index")
	beego.Router("/index", &controllers.MainController{}, "get:Index")
	beego.Router("/overview", &controllers.MainController{}, "get:Overview")
	beego.Router("/OnlineUsersIndex", &controllers.MainController{}, "get:OnlineUsersIndex")
	beego.Router("/TrainingProcessesIndex", &controllers.MainController{}, "get:TrainingProcessesIndex")
	beego.Router("/TrainingResultsIndex", &controllers.MainController{}, "get:TrainingResultsIndex")
	beego.Router("/DesktopSharesIndex", &controllers.MainController{}, "get:DesktopSharesIndex")

	beego.Router("/test", &controllers.MainController{}, "get:Test")

	beego.Router("/user/online", &controllers.MainController{}, "get:Online")
	beego.Router("/user/offline", &controllers.MainController{}, "get:Offline")
	beego.Router("/user/list", &controllers.MainController{}, "get:UserList")

	beego.Router("/desktopshare/upload", &controllers.MainController{}, "post:UploadDesktopShare")
	beego.Router("/desktopshare/list", &controllers.MainController{}, "get:DesktopShares")

	beego.Router("/trainingprocess/add", &controllers.MainController{}, "get:AddTrainingProcess")
	beego.Router("/trainingprocess/remove", &controllers.MainController{}, "post:RemoveTrainingProcess")
	beego.Router("/trainingprocess/list", &controllers.MainController{}, "get:TrainingProcesses")

	beego.Router("/trainingresult/add", &controllers.MainController{}, "get:AddTrainingResult")
	beego.Router("/trainingresult/remove", &controllers.MainController{}, "post:RemoveTrainingResults")
	beego.Router("/trainingresult/list", &controllers.MainController{}, "get:TrainingResults")

}
