<!DOCTYPE html>
<html>
<head>
    <title>生产厂商列表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link href="/stylesheets/index-theme.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="/easyUI/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/easyUI/themes/icon.css">
    <style type="text/css">
        .demo-info {
            background: #FFFEE6;
            color: #8F5700;
            padding: 12px;
        }
        .demo-tip {
            width: 16px;
            height: 16px;
            margin-right: 8px;
            float: left;
        }

    </style>
    <script src="/javascripts/jquery.min.js" type="text/javascript"></script> 
    <script type="text/javascript" src="/easyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/easyUI/locale/easyui-lang-zh_CN.js"></script>
    <script src="/javascripts/tools.js" type="text/javascript"></script>  
</head>
<body>
    <h2>生产厂商列表</h2>
    <div class="demo-info">
        <div class="demo-tip icon-tip"></div>
        <div>选中厂商进行删除和编辑操作；如果不存在该厂商，可以添加</div>
    </div>
    <div style="margin:10px 0;"></div>
    

    <div id="tb">
        <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="refresh_grid()">刷新</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="insert()">增加</a>
        <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteRows()">删除</a>
        <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="test()">测试</a>         -->
    </div>
    <table id = "grid1"></table>

    <script type="text/javascript">
    var toolbar = $('#tb');
    var grid1 = $('#grid1');

    $(function(){
            $('#grid1').datagrid({
                title:'列表',
                iconCls:'icon-edit',
                width:'auto',
                height:'auto',
                fitColumns:true,
                singleSelect:true,
                idField:'producerCode',
                toolbar: '#tb',
                url:'/producerList',
                columns:[[
                    {field:'producerCode',title:'生产厂商编码',width:60,align:'center',
                        editor:{
                            type:'text',
                            options:{
                                required:true
                            }
                        }
                    },

                    {field:'producerName',title:'生产厂商名称',width:80,align:'center',
                        editor:{
                            type:'text',
                            options:{
                                required:true
                            }
                        }
                    },
                    {field:'timeStamp',title:'创建时间',width:80,align:'center',
                        editor:{
                            // type:'text',
                            options:{
                                editable:false,
                                required:false
                            }
                        }
                    },
                    {field:'note',title:'备注',width:100,align:'center',
                        editor:{
                            type:'text',
                            options:{
                                required:true
                            }
                        }
                    },

                    // {field:'resource_note',title:'说明',width:180,editor:'text'},
                    {field:'action',title:'',width:40,align:'center',
                        formatter:function(value,row,index){
                            if (row.editing){
                                var s = '<a href="#" onclick="saverow(this)">保存</a> ';
                                var c = '<a href="#" onclick="cancelrow(this)">取消</a>';
                                return s+c;
                            }
                            //  else {
                            //     var e = '<a href="#" onclick="editrow(this)">编辑</a> ';
                            //     return e;
                            // }
                        }
                    }
                ]],
                // rowStyler: function(index,row){
                //     if ((index % 2) != 0){
                //         return 'background-color:#f3f3f3;color:#000;';
                //     }
                // },

                onBeforeEdit:function(index,row){
                    row.editing = true;
                    updateActions(index);
                },
                onAfterEdit:function(index,row){
                    row.editing = false;
                    updateActions(index);
                },
                onCancelEdit:function(index,row){
                    row.editing = false;
                    updateActions(index);
                }
            });
        });

    function updateActions(index){
        grid1.datagrid('updateRow',{
            index: index,
            row:{}
        });
    }
    function getRowIndex(target){
        var tr = $(target).closest('tr.datagrid-row');
        return parseInt(tr.attr('datagrid-row-index'));
    }
    function editrow(target){
        grid1.datagrid('beginEdit', getRowIndex(target));
    }

    function saverow(target)
    {
        var index = getRowIndex(target);
        grid1.datagrid('endEdit', index);
        var rows = grid1.datagrid('getRows');
        var row = rows[index];
        if(row.producerCode == undefined || row.producerCode == '' || row.producerName == undefined || row.producerName == '' 
            || row.note == undefined || row.note == ''){
            $.messager.alert('提示','生产厂商的编码、名称和描述都是必须填写的！','info');
            grid1.datagrid('beginEdit', index);
            return;
        }
        else{
            var json = {producerCode: row.producerCode, producerName: row.producerName, note: row.note};
            $.ajax({
                url: "/addProducer",
                data: json,
                type: "post",
                // contentType: "text/plain",
                success: function (text) {
                    if(text == 'ok'){
                        $.messager.alert('提示','操作成功！');               
                        refresh_grid();                        
                    } 
                    else if(text == 'duplicated') {
                        $.messager.alert('提示','厂商编码或者名称重复添加！', 'warning');             
                    }else{
                        $.messager.alert('提示','操作出现异常！', 'warning');             
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });
            refresh_grid();
        }
    }
    function cancelrow(target){
        refresh_grid();
    }
    function refresh_grid()
    {
        grid1.datagrid('reload');
    }        
    function insert(){
        var row = grid1.datagrid('getSelected');
        if (row){
            var index = grid1.datagrid('getRowIndex', row);
        } else {
            index = 0;
        }
        if(index < 0) index = 0;
        grid1.datagrid('insertRow', {
            index: index,
            row:{
                // status:'P'
            }
        });
        grid1.datagrid('selectRow',index);
        grid1.datagrid('beginEdit',index);
    }
    function deleteRows()
    {
        $.messager.confirm('确认','删除该信息将无法恢复，确定删除吗？',function(r){
            if (r){
                var rows = grid1.datagrid('getSelections');
                if(rows.length > 0){
                    var json = {producerCode: rows[0].producerCode};
                    $.ajax({
                        url: "/removeProducer",
                        data: json,
                        type: "post",
                        success: function (text) {
                            if(text == 'ok'){
                                $.messager.alert('提示','操作成功！');               
                                refresh_grid();                        
                            }else{
                                $.messager.alert('提示','操作出现异常！', 'warning');             
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.responseText);
                        }
                    });                
                }

            }
        });     
    }   
    </script>
    <div class="description">
        <h3>说明</h3>
        <p></p>
    </div>


 
</body>
</html>