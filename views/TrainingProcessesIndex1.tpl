<!DOCTYPE html>
<html>
<head>
    <title>实验进程列表</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" href="/easyUI/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="/easyUI/themes/icon.css">
    <link href="/stylesheets/index-theme.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .demo-info {
            background: #FFFEE6;
            color: #8F5700;
            padding: 12px;
        }
        .demo-tip {
            width: 16px;
            height: 16px;
            margin-right: 8px;
            float: left;
        }

    </style>
    <script src="/javascripts/jquery.min.js" type="text/javascript"></script> 
    <script type="text/javascript" src="/easyUI/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/easyUI/locale/easyui-lang-zh_CN.js"></script>
    <script src="/javascripts/tools.js" type="text/javascript"></script>  
</head>
<body>
    <h2>实验进程列表</h2>
    <div class="demo-info">
        <div class="demo-tip icon-tip"></div>
        <div>查看各个实验箱的实验进程情况</div>
    </div>
    <div style="margin:10px 0;"></div>
    

    <div id="tb">
        <a href="#" class="easyui-linkbutton" iconCls="icon-reload" plain="true" onclick="refresh_grid()">刷新</a>
        <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="insert()">增加</a> -->
        <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteRows()">删除</a> -->
        <!-- <a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="test()">测试</a>         -->
    </div>
    <table id = "grid1"></table>

    <script type="text/javascript">
    var toolbar = $('#tb');
    var grid1 = $('#grid1');
    var itemTypes = []

    $(function(){
            $('#grid1').datagrid({
                title:'列表',
                iconCls:'icon-edit',
                width:'auto',
                height:'auto',
                fitColumns:true,
                singleSelect:true,
                idField:'ID',
                toolbar: '#tb',
                url:'/trainingprocess/list',
                columns:[[
                    {field:'ID',title:'实验箱编码',width:30,align:'center',
                        editor:{
                            type:'text',
                            options:{
                                required:true
                            }
                        }
                    },

                    {field:'ProcessName',title:'当前实验进程',width:40,align:'center'},
                    {field:'Time',title:'开始时间',width:30,editor:'text',align:'center'}
                ]]
            });
        });

    function updateActions(index){
        grid1.datagrid('updateRow',{
            index: index,
            row:{}
        });
    }
    function getRowIndex(target){
        var tr = $(target).closest('tr.datagrid-row');
        return parseInt(tr.attr('datagrid-row-index'));
    }
    function editrow(target){
        grid1.datagrid('beginEdit', getRowIndex(target));
    }

    function saverow(target)
    {
        var index = getRowIndex(target);
        grid1.datagrid('endEdit', index);
        var rows = grid1.datagrid('getRows');
        var row = rows[index];
        if(row.epc == undefined || row.epc == '' || row.productType == undefined || row.productType == ''){
            $.messager.alert('提示','EPC编码和产品类型是必须填写的！','info');
            grid1.datagrid('beginEdit', index);
            return;
        }
        else{
            var json = {epc: row.epc, productType: row.productType};
            $.ajax({
                url: "/addEpc",
                data: json,
                // data: { data: JSON.stringify(json) },
                type: "post",
                // contentType: "text/plain",
                success: function (text) {
                    // alert(text);
                    if(text == "ok")
                    {
                        $.messager.alert('提示','操作成功！');               
                    }
                    else if(text == 'duplicated')
                    {
                        $.messager.alert('提示','EPC重复添加！', 'warning');             
                    }else{
                        $.messager.alert('提示','操作出现异常！', 'warning');             

                    }
                    refresh_grid();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseText);
                }
            });
            refresh_grid();
        }
    }
    function cancelrow(target){
        refresh_grid();
    }
    function refresh_grid()
    {
        grid1.datagrid('reload');
    }        
    function insert(){
        var row = grid1.datagrid('getSelected');
        if (row){
            var index = grid1.datagrid('getRowIndex', row);
        } else {
            index = 0;
        }
        if(index < 0) index = 0;
        grid1.datagrid('insertRow', {
            index: index,
            row:{
                // status:'P'
            }
        });
        grid1.datagrid('selectRow',index);
        grid1.datagrid('beginEdit',index);
    }
    function deleteRows()
    {
        $.messager.confirm('确认','删除该信息将无法恢复，确定删除吗？',function(r){
            if (r){
                var rows = grid1.datagrid('getSelections');
                if(rows.length > 0){
                    var json = {epc: rows[0].epc};
                    $.ajax({
                        url: "/removeEpc",
                        contentType: "text/plain",
                        data: JSON.stringify(json),
                        type: "post",
                        success: function (text) {
                            if(text == "ok")
                            {
                                $.messager.alert('提示','操作成功！','info');
                            }
                            else
                            {
                                $.messager.alert('提示','操作出现异常！','info');
                            }
                            refresh_grid();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert(jqXHR.responseText);
                            refresh_grid();
                        }
                    });                
                    refresh_grid();                    
                }

            }
        });     
    }   
    </script>
    <div class="description">
        <h3>说明</h3>
        <p></p>
    </div>


 
</body>
</html>